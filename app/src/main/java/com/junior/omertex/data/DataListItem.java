package com.junior.omertex.data;


import com.junior.omertex.api.response.Photo;
import com.junior.omertex.api.response.Post;

import java.io.Serializable;

public class DataListItem {

    private Photo mPhoto;
    private Post mPost;

    public DataListItem(Photo photo, Post post){
        mPhoto = photo;
        mPost = post;
    }

    public Photo getPhoto() {
        return mPhoto;
    }

    public void setPhoto(Photo photo) {
        mPhoto = photo;
    }

    public Post getPost() {
        return mPost;
    }

    public void setPost(Post post) {
        mPost = post;
    }
}

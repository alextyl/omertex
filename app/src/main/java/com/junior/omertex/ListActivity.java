package com.junior.omertex;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.junior.omertex.api.response.Photo;
import com.junior.omertex.api.response.Post;
import com.junior.omertex.application.App;
import com.junior.omertex.data.DataListItem;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;


public class ListActivity extends AppCompatActivity {

    private ListView mListView;
    private Handler mHandler;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {

                Realm realm = Realm.getDefaultInstance();

                try {
                    List<Photo> photoList = App.getFlickrApi().getPhotos().execute().body().getPhotos().getPhoto();
                    realm.beginTransaction();
                    realm.where(Photo.class).findAll().deleteAllFromRealm();
                    realm.commitTransaction();
                    for(final Photo photo : photoList){
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(photo);
                            }
                        });
                    }
                    List<Post> postList = App.getJsonPlaceholderApi().getPosts().execute().body();
                    realm.beginTransaction();
                    realm.where(Post.class).findAll().deleteAllFromRealm();
                    realm.commitTransaction();
                    for(final Post post : postList){
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(post);
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    mHandler.sendEmptyMessage(0);
                } finally {
                    realm.close();
                }

                mHandler.sendEmptyMessage(1);
            }
        });

        mHandler = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {

                mProgressBar.setVisibility(View.GONE);
                mListView.setVisibility(View.VISIBLE);

                switch (message.what){
                    case 1: {
                        setAdapter();
                        return true;
                    }
                    default: {
                        Toast.makeText(getApplicationContext(), R.string.error, Toast.LENGTH_SHORT).show();
                        return false;
                    }
                }
            }
        });

        mListView = findViewById(R.id.list_view);
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                DataListItem dataListItem = (DataListItem) mListView.getAdapter().getItem(i);
                Intent intent = new Intent(ListActivity.this, DetailActivity.class);
                intent.putExtra("url", dataListItem.getPhoto().getUrlZ());
                intent.putExtra("photoTitle", dataListItem.getPhoto().getTitle());
                intent.putExtra("postTitle", dataListItem.getPost().getTitle());
                intent.putExtra("postBody", dataListItem.getPost().getBody());
                startActivity(intent);
            }
        });

        mProgressBar = findViewById(R.id.progressBar);

        thread.start();
    }

    private class PhotoListAdapter extends ArrayAdapter<DataListItem> {


        public PhotoListAdapter(Context context, ArrayList<DataListItem> dataListItems){
            super(context, 0, dataListItems);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

            final DataListItem dataListItem = getItem(position);

            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_view_item, parent, false);
            }

            TextView title = (TextView) convertView.findViewById(R.id.text_view_1);
            TextView url = (TextView) convertView.findViewById(R.id.text_view_2);

            title.setText(dataListItem.getPost().getTitle());
            url.setText(dataListItem.getPhoto().getUrlZ());

            return convertView;
        }
    }


    private void setAdapter(){
        Realm realm = Realm.getDefaultInstance();
        RealmResults<Photo> photos = realm.where(Photo.class).findAll();
        RealmResults<Post> posts = realm.where(Post.class).findAll();

        ArrayList<DataListItem> dataListItems = new ArrayList<>();


        for (int i = 0; i < posts.size(); i++){
            dataListItems.add(new DataListItem(photos.get(i), posts.get(i)));
        }

        mListView.setAdapter(new PhotoListAdapter(getApplicationContext(), dataListItems));

    }
}

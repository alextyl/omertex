package com.junior.omertex.api;


import com.junior.omertex.api.response.FlickerResponse;

import retrofit2.Call;
import retrofit2.http.GET;

public interface FlickrApi {

    @GET("/services/rest?method=flickr.photos.getRecent&api_key=843240ad62ddf7acfdc74e9d1eb02e87&format=json&nojsoncallback=1&extras=url_z")
    Call<FlickerResponse> getPhotos();


}
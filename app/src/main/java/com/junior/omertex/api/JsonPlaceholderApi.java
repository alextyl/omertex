package com.junior.omertex.api;

import com.junior.omertex.api.response.Post;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface JsonPlaceholderApi {

    @GET("/posts")
    Call<List<Post>> getPosts();

}

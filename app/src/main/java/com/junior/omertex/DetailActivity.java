package com.junior.omertex;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.junior.omertex.data.DataListItem;
import com.squareup.picasso.Picasso;

public class DetailActivity extends AppCompatActivity {

    private ImageView mImageView;
    private TextView mTextViewTitlePhoto;
    private TextView mTextViewTitltePost;
    private TextView mTextViewBodyPost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        Intent intent = getIntent();
        String url = intent.getStringExtra("url");
        String photoTitle = intent.getStringExtra("photoTitle");
        String postTitle = intent.getStringExtra("postTitle");
        String postBody = intent.getStringExtra("postBody");

        mTextViewTitlePhoto = findViewById(R.id.textView_1);
        if(photoTitle != null) mTextViewTitlePhoto.setText(photoTitle);

        mTextViewTitltePost = findViewById(R.id.textView2);
        if(postTitle != null) mTextViewTitltePost.setText(postTitle);

        mTextViewBodyPost = findViewById(R.id.textView3);
        if(postBody != null) mTextViewBodyPost.setText(postBody);

        mImageView = findViewById(R.id.imageView);
        Picasso.with(this)
                .load(url)
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.errorstop)
                .into(mImageView);
    }
}

package com.junior.omertex.application;


import android.app.Application;


import com.junior.omertex.api.FlickrApi;
import com.junior.omertex.api.JsonPlaceholderApi;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class App extends Application {

    private Retrofit retrofit;
    private static FlickrApi flickrApi;
    private static JsonPlaceholderApi jsonPlaceholderApi;


    @Override
    public void onCreate() {
        super.onCreate();

        retrofit = new Retrofit.Builder()
                .baseUrl("https://www.flickr.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        flickrApi = retrofit.create(FlickrApi.class);

        retrofit = new Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        jsonPlaceholderApi = retrofit.create(JsonPlaceholderApi.class);

        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder().name("omertex.realm")
                .schemaVersion(0)
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);

    }


    public static FlickrApi getFlickrApi() {
        return flickrApi;
    }

    public static JsonPlaceholderApi getJsonPlaceholderApi() {
        return jsonPlaceholderApi;
    }
}
